var wms_layers = [];


        var lyr_GoogleMaps_0 = new ol.layer.Tile({
            'title': 'Google Maps',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
            })
        });

        var lyr_GoogleSatellite_1 = new ol.layer.Tile({
            'title': 'Google Satellite',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}'
            })
        });

        var lyr_OpenStreetMap_2 = new ol.layer.Tile({
            'title': 'OpenStreetMap',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
        });

        var lyr_MaxarAccraMosaic2019_3 = new ol.layer.Tile({
            'title': 'Maxar Accra Mosaic 2019',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tiles.openaerialmap.org/5ea05b54411bed0005680396/0/5ea05b54411bed0005680397/{z}/{x}/{y}'
            })
        });
var format_200MetersBuffer_4 = new ol.format.GeoJSON();
var features_200MetersBuffer_4 = format_200MetersBuffer_4.readFeatures(json_200MetersBuffer_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_200MetersBuffer_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_200MetersBuffer_4.addFeatures(features_200MetersBuffer_4);
var lyr_200MetersBuffer_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_200MetersBuffer_4, 
                style: style_200MetersBuffer_4,
                interactive: true,
                title: '<img src="styles/legend/200MetersBuffer_4.png" /> 200 Meters Buffer'
            });
var format_OdawDrains_5 = new ol.format.GeoJSON();
var features_OdawDrains_5 = format_OdawDrains_5.readFeatures(json_OdawDrains_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_OdawDrains_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_OdawDrains_5.addFeatures(features_OdawDrains_5);
var lyr_OdawDrains_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_OdawDrains_5, 
                style: style_OdawDrains_5,
                interactive: true,
                title: '<img src="styles/legend/OdawDrains_5.png" /> Odaw Drains'
            });
var format_FlightPaths_6 = new ol.format.GeoJSON();
var features_FlightPaths_6 = format_FlightPaths_6.readFeatures(json_FlightPaths_6, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_FlightPaths_6 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_FlightPaths_6.addFeatures(features_FlightPaths_6);
var lyr_FlightPaths_6 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_FlightPaths_6, 
                style: style_FlightPaths_6,
                interactive: true,
    title: 'Flight Paths<br />\
    <img src="styles/legend/FlightPaths_6_0.png" /> 1<br />\
    <img src="styles/legend/FlightPaths_6_1.png" /> 2<br />\
    <img src="styles/legend/FlightPaths_6_2.png" /> 3<br />\
    <img src="styles/legend/FlightPaths_6_3.png" /> 4<br />'
        });

lyr_GoogleMaps_0.setVisible(false);lyr_GoogleSatellite_1.setVisible(false);lyr_OpenStreetMap_2.setVisible(true);lyr_MaxarAccraMosaic2019_3.setVisible(true);lyr_200MetersBuffer_4.setVisible(true);lyr_OdawDrains_5.setVisible(true);lyr_FlightPaths_6.setVisible(true);
var layersList = [lyr_GoogleMaps_0,lyr_GoogleSatellite_1,lyr_OpenStreetMap_2,lyr_MaxarAccraMosaic2019_3,lyr_200MetersBuffer_4,lyr_OdawDrains_5,lyr_FlightPaths_6];
lyr_200MetersBuffer_4.set('fieldAliases', {'FID': 'FID', 'AREA_KM_SQ': 'AREA_KM_SQ', });
lyr_OdawDrains_5.set('fieldAliases', {'Drain': 'Drain', 'DrainName': 'DrainName', 'Level': 'Level', 'Task3': 'Task3', 'waterway': 'waterway', 'capture': 'capture', 'path': 'path', });
lyr_FlightPaths_6.set('fieldAliases', {'path': 'path', 'LENGTH_KM': 'LENGTH_KM', });
lyr_200MetersBuffer_4.set('fieldImages', {'FID': '', 'AREA_KM_SQ': '', });
lyr_OdawDrains_5.set('fieldImages', {'Drain': 'TextEdit', 'DrainName': 'TextEdit', 'Level': 'TextEdit', 'Task3': 'TextEdit', 'waterway': 'TextEdit', 'capture': 'TextEdit', 'path': 'Range', });
lyr_FlightPaths_6.set('fieldImages', {'path': 'Range', 'LENGTH_KM': 'TextEdit', });
lyr_200MetersBuffer_4.set('fieldLabels', {'FID': 'no label', 'AREA_KM_SQ': 'header label', });
lyr_OdawDrains_5.set('fieldLabels', {'Drain': 'no label', 'DrainName': 'no label', 'Level': 'no label', 'Task3': 'no label', 'waterway': 'no label', 'capture': 'no label', 'path': 'no label', });
lyr_FlightPaths_6.set('fieldLabels', {'path': 'no label', 'LENGTH_KM': 'header label', });
lyr_FlightPaths_6.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});